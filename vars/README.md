# How to execute ansible role.


### Step-1 Go to the inventory file and add ip address and user name in which you want to execute role.

***
![Screenshot__1464_](/uploads/589c2735b309b2d94e0065daca2731b7/Screenshot__1464_.png)
***

### Step-2 Go to the jenkins and add credentials using pem key.

***
![image__1_](/uploads/7f5118319290083c86e5749887ab9bfe/image__1_.png)
***

### Step-3 Go to the jenkins file and add this credentials name in credentialsId under Dryrun plabook and Execute Playbook stage.

***
![Screenshot__1469_](/uploads/08f8a6bda1804087e76b1324d43bfb6e/Screenshot__1469_.png)
***

***
![Screenshot__1470_](/uploads/d58a52d2a2334ebe025f27569d39872a/Screenshot__1470_.png)
***


### Step-4 Go to the pubkeyonboardinglist file and add user name, group name, present. These should be separated by coma.

***
![Screenshot__1467_](/uploads/5d0498024389c5208b562d6230b31753/Screenshot__1467_.png)
***


### Step-5 Then go to the pub_keys directory and create a file with the same user name, you mentioned in pubkeyonboardinglist file and add that user's id_rsa.pub key in this file.

***
![Screenshot__1465_](/uploads/505ab543caf5a505f6dfaa89de42fcb3/Screenshot__1465_.png)
***

***
![Screenshot__1466_](/uploads/07a52e9367e910a62cefbb4d356ede0e/Screenshot__1466_.png)
***

### Step-6 Now you can execute your jenkins file.

